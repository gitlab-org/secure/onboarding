# Realignment Information

* **Date of Realignment: `YYYY-MM-DD`**
* **Team Member Name: `Team member GitLab handle`**
* Current Group: `Group name`
* Current Engineering Manager: `EM GitLab handle`
* New Group: `Group name`
* New Engineering Manager: `EM GitLab handle`

---

## Current Manager

* [ ] **Schedule** a meeting to discuss the transition with the affected team member.
* [ ] **Post** a Goodbye / Thank you message to the group Slack channel.
* [ ] **Verify** Expense Software is updated with the correct Manager.
* [ ] **Verify** Workday is updated with the correct Manager.
* [ ] **Make a copy** of the [career mobility document template](https://docs.google.com/document/d/1iRbmS518CDjmhZEjvkaibqR1g0angWE83sWvtlH-elE/edit?usp=sharing) for this team member.
* [ ] **Document accomplishments** from this fiscal year in the career mobility document. _Please be specific, and include issue, MR, and handbook links when applicable._
* [ ] **Document Strengths** from this fiscal year in the career mobility document. _Share a few of the team member’s most prominent strengths and the impact this has had in their role, on their team, and at the company level (if applicable). Feel free to reference our [competencies](https://about.gitlab.com/handbook/competencies/) and our [values_](https://about.gitlab.com/handbook/values/)._
* [ ] **Document Improvement Areas** from this fiscal year in the career mobility document. _Everyone has improvement areas; awareness of these areas is key for continued growth and development. Please outline a few of the team member’s improvement areas and any applicable plans in place for development and improvement. Feel free to reference our [competencies](https://about.gitlab.com/handbook/competencies/) and our [values](https://about.gitlab.com/handbook/values/)._
* [ ] **Document Goals** from this fiscal year in the career mobility document. _Please outline the transitioning team member’s goals and career aspirations for transparency and to ensure alignment with the new reporting manager to discuss how to continue supporting the team member with these goals._
* [ ] **Schedule** a meeting with the new manager to discuss the completed career mobility document.
* [ ] **Remove** the new team member from all of the relevant Calendar Appointments.

## New Manager

* [ ] **Schedule a meeting** to discuss the transition with the affected team member.
* [ ] **Post a Welcome** message to group Slack channel.
* [ ] **Verify or update** the GitLab Org Chart is updated.
* [ ] **Update or verify Workday has been updated** with the new team member.
  * [ ] Update or verify `Job Title Specialty` (Job -> Organizations) is updated to the new group.
* [ ] **Add** the new team member to all of the relevant Slack Channels.
* [ ] **Add** the new team member to all of the relevant Calendar Appointments.
* [ ] **Schedule 1 to 1 Meetings** with the new team member.
* [ ] **Review [Product Docs](https://docs.gitlab.com/ee/)** for new categories you might have acquired.
  * [ ] **Provide guidance** on how the team member should communicate and collaborate with their new team.
* [ ] **Provide ongoing support** to the team members during the transition period, including regular check-ins and feedback sessions.
* [ ] Share your current OKRs.
* [ ] **Share** group specific Handbook pages.
* [ ] **Share** group specific processes.
* [ ] **Share** group specific communication methods (Weekly Announcements, etc).

## Team Member

* [ ] **Meet** with your new manager.
* [ ] **Share your Career Development** / goals with your new manager.
* [ ] **Review** the new team's Handbook and Docs Pages ( [Direction Pages](https://about.gitlab.com/direction/#devsecops-stages), [Group & Category Information](https://about.gitlab.com/handbook/product/categories/),[Product Docs for the new categories supported by your group and stage](https://docs.gitlab.com/ee/)).
* [ ] **Share** your work hours (including any country laws related to work schedule).
* [ ] **Document your accomplishments** completed since your last performance review.
* [ ] **Document areas of improvements** you are currently working on.
